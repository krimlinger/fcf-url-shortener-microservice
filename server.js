"use strict";

const express = require("express");
const mongo = require("mongodb");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const crypto = require("crypto");
const dns = require("dns");
const cors = require("cors");
const url = require("url");

const app = express();

const port = process.env.PORT || 3000;

function genHash(url) {
  const shaSum = crypto.createHash("sha256");
  shaSum.update(url);
  return shaSum.digest("base64").substr(0, 8);
}

function selectAPIProps({ original_url, short_url }) {
  return { original_url, short_url };
}

const shortURLSchema = new mongoose.Schema({
  original_url: String,
  short_url: String
});

const ShortURL = mongoose.model("URL", shortURLSchema);

mongoose.connect(
  process.env.MONGO_URI,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function(err) {
    if (err) {
      console.log(err);
      throw err;
    }
  }
);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/public", express.static(process.cwd() + "/public"));

app.get("/", function(req, res) {
  res.sendFile(process.cwd() + "/views/index.html");
});

app.post("/api/shorturl/new", function(req, res) {
  const origURL = req.body.url;
  let hostname = "";
  try {
    hostname = url.parse(origURL).hostname;
  } catch (e) {
    if (e instanceof TypeError) {
      return res.json({ error: "invalid URL" });
    }
    throw e;
  }
  dns.lookup(hostname, err => {
    if (err) {
      return res.json({ error: "invalid URL" });
    }
    findShortURL();
  });
  const findShortURL = () => {
    const shortURL = genHash(origURL);
    ShortURL.findOne({ short_url: shortURL }, (err, url) => {
      if (err) {
        console.log(err);
        return res.json({ error: "internal error" });
      } else if (!url) {
        url = new ShortURL({ original_url: origURL, short_url: shortURL });
        url.save();
      }
      res.json(selectAPIProps(url));
    });
  };
});

app.get("/api/shorturl/:short_url", function(req, res) {
  const shortURL = req.params.short_url;
  ShortURL.findOne({ short_url: shortURL }, (err, url) => {
    if (err) {
      console.log(err);
      return res.json({ error: "internal error" });
    } else if (url === null) {
      return res.json({ error: "target short URL does not exist" });
    }
    let urlWithProtocol = url.original_url;
    if (urlWithProtocol.slice(0, 4) !== "http") {
      urlWithProtocol = "https://" + urlWithProtocol;
    }
    res.redirect(urlWithProtocol);
  });
});

app.listen(port, function() {
  console.log("Node.js listening ...");
});
